# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [Unreleased]

### Changed
- Changed job file to use .hcl extension to match Nomad reccomendations

### Added
- Initial pipeline created to deploy Linux AMD64 variant
