# GitLab Linux AMD64 Runner

[[_TOC_]]

## Description

A deployment job for the [GitLab Nomad Runner Container](https://gitlab.com/carboncollins-cloud/cicd/gitlab-nomad-runner-container) targeting Linux AMD64 systems.

## Deployments

The contents of this repo do not get directly used and instead deployments are triggered from the parent reposiotry [GitLab Nomad Runner - Base Container](https://gitlab.com/carboncollins-cloud/cicd/gitlab-nomad-runner-container) when changes are made to the base container image. This trigger will cause the Nomad Job file contained within this repo to be deployed.

## Other Info

This Nomad job file is spun off as a seperate repository as it is intended that each platform will have its own Nomad job. It is also seperated due to the limitation of 1 job file in the common [GitLab Pipeline Template](https://gitlab.com/carboncollins-cloud/cicd/gitlab-pipeline-template) used by all [CarbonCollins - Cloud](https://gitlab.com/carboncollins-cloud) projects.

I am currently running this in my homelab as an experiment and as a learning exercise, so due to this I wouldn’t recommend it for any real production loads... It can be used as an example of one way of doing this :D 
