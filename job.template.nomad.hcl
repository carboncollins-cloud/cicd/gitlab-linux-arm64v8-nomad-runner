job "cicd-gitlab-linux-arm64v8-nomad-runner" {
  name = "GitLab Nomad Runner (Linux ARM64v8)"
  type = "service"
  region = "se"
  datacenters = ["soc"]
  namespace = "c3-cicd-gitlab"

  group "runner" {
    count = 2

    spread {
      attribute = "${node.node.unique.name}"
      weight = 50
    }

    constraint {
      attribute = "${attr.cpu.arch}"
      value = "arm64"
    }

    constraint {
      attribute = "${attr.kernel.name}"
      value = "linux"
    }

    task "runner" {
      driver = "docker"

      kill_signal = "SIGINT"
      kill_timeout = "630s"

      vault {
        policies = ["service-gitlab-nomad-runner"]

        change_mode   = "signal"
        change_signal = "SIGINT"
      }

      config {
        image = "[[ .runnerImageName ]]"
      }

      env {
        TZ = "[[ .defaultTimezone ]]"
        CONFIG_FILE = "${NOMAD_ALLOC_DIR}/gitlab-runner-config.toml"
        CI_SERVER_URL = "https://gitlab.com/"

        RUNNER_NAME = "${NOMAD_ALLOC_ID}"
        RUNNER_USER = "gitlab-runner"
        RUNNER_WORKING_DIR = "/home/gitlab-runner"
        RUNNER_PLATFORM = "linux-arm64v8"
        RUNNER_TAGS = "nomad,terraform,consul,vault,levant,jq,isolated,${NOMAD_DC},${NOMAD_REGION}"

        EXECUTOR_NOMAD_REGION = "se"
        EXECUTOR_NOMAD_NAMESPACE = "c3-cicd-gitlab"
        EXECUTOR_NOMAD_JOB_ID = "cicd-gitlab-linux-arm64v8-nomad-executor"

        AUTH_TYPE = "approle+nomad-backend"
        APPROLE_LOGIN_PATH = "cicd"
        APPROLE_NOMAD_BACKEND_PATH = "nomad/creds/cicd-gitlab-nomad-runner"

        NOMAD_NAMESPACE = "c3-cicd-gitlab"
        NOMAD_REGION = "se"
        NOMAD_ADDR = "[[ .nomadAddress ]]"

        // Skip SSL is temp until bootstrapped fully
        VAULT_ADDR = "[[ .vaultAddress ]]"
        VAULT_SKIP_VERIFY = "true"

        // Skip SSL is temp until bootstrapped fully
        CONSUL_HTTP_ADDR = "[[ .consulAddress ]]"
        CONSUL_HTTP_SSL = "true"
        CONSUL_HTTP_SSL_VERIFY = "false"
      }

      template {
        data = <<EOH
          {{ with secret "c3kv/data/api/gitlab/carboncollins-cloud/runner" }}
          RUNNER_REGISTRATION_TOKEN={{ index .Data.data "runnerRegistrationToken" }}
          {{ end }}

          {{ with secret "auth/cicd/role/cicd-gitlab-nomad-runner/secret-id" "secret_id_num_uses=1" "secret_id_ttl=5m" }}
          APPROLE_SECRET_ID={{ .Data.secret_id }}
          {{ end }}
        EOH

        destination = "${NOMAD_SECRETS_DIR}/runner.env"
        change_mode = "noop"
        env = true
      }
    }
  }

  reschedule {
    delay = "10s"
    delay_function = "exponential"
    max_delay = "10m"
    unlimited = true
  }

  update {
    max_parallel = 1
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "10m"
    progress_deadline = "15m"
    auto_revert       = true
    auto_promote      = true
    stagger = "30s"
    canary = 1
  }

  meta {
    gitSha = "[[ .gitSha ]]"
    gitBranch = "[[ .gitBranch ]]"
    pipelineId = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId = "[[ .projectId ]]"
    projectUrl = "[[ .projectUrl ]]"
  }
}
